import React, { useState } from "react";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import "./style.css";
import AddTech from "../AddTech";
import EditTech from "../EditTech";
import axios from "axios";
import { Button } from "@material-ui/core";

const Techs = ({ techs, handleUser }) => {
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [idTech, setIdtech] = useState(false);

  const openModalAdd = () => {
    setOpen(!open);
  };

  const deleteTech = (id) => {
    const sessionToken = sessionStorage.getItem("token");
    axios
      .delete(`https://kenziehub.me/users/techs/${id}`, {
        headers: { Authorization: `Bearer ${JSON.parse(sessionToken)}` },
      })
      .then((response) => {
        handleUser();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const editTech = (techId) => {
    setOpenEdit(!openEdit);
    setIdtech(techId);
  };
  return (
    <>
      <h3>Techs</h3>
      <hr />
      <EditTech
        openEdit={openEdit}
        setOpenEdit={setOpenEdit}
        id={idTech}
        handleUser={handleUser}
      ></EditTech>
      <AddTech open={open} setOpen={setOpen} handleUser={handleUser}></AddTech>
      <div id="techs-add">
        {techs &&
          techs.map((tech, index) => {
            return (
              <p key={index} id={tech.id} className="tech">
                <span>{tech.title + ":"}</span>
                {tech.status}

                <Tooltip
                  title="Edit"
                  id="edit-icon"
                  onClick={() => {
                    editTech(tech.id);
                  }}
                >
                  <IconButton aria-label="edit">
                    <EditIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip
                  title="Delete"
                  id="delete-icon"
                  onClick={() => {
                    deleteTech(tech.id);
                  }}
                >
                  <IconButton aria-label="edit">
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </p>
            );
          })}
      </div>

      <div id="btn-add">
        <Button
          id="add"
          className="add"
          onClick={() => {
            openModalAdd();
          }}
        >
          Add
        </Button>
      </div>
    </>
  );
};

export default Techs;
