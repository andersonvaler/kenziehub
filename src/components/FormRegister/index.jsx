import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { createMuiTheme, styled } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { useState } from "react";
import MuiAlert from "@material-ui/lab/Alert";
import { Button, TextField } from "@material-ui/core";
const Text = styled(TextField)({
  margin: "5px",
  width: "300px",
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const FormRegister = () => {
  const history = useHistory();
  const [error, setError] = useState(false);

  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    name: yup.string().required("Campo obrigatório"),
    bio: yup.string().required("Campo obrigatório"),
    course_module: yup.string().required("Campo obrigatório"),
    contact: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos uma letra maiúscula, uma minúscula, um número e um caracter especial!"
      )
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    axios
      .post("https://kenziehub.me/users", data)
      .then((response) => {
        reset();
        history.push("/");
      })
      .catch((error) => {
        const { status, message } = error.response.data;
        if (status === "error") {
          setError(message);
          reset();
        }
      });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#284B63",
      },
      secondary: {
        main: "#153243",
      },
      error: {
        main: "#FF220C",
      },
    },
  });

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <h2>Cadastro</h2>

      <ThemeProvider theme={theme}>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Email"
            name="email"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
            id="input"
          />
        </div>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Nome"
            name="name"
            size="small"
            color="primary"
            erio
            inputRef={register}
            error={!!errors.name}
            helperText={errors.name?.message}
            id="input"
          />
        </div>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Senha"
            name="password"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
            id="input"
            type="password"
          />
        </div>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Bio"
            name="bio"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.bio}
            helperText={errors.bio?.message}
            id="input"
          />
        </div>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Contato"
            name="contact"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.contact}
            helperText={errors.contact?.message}
            id="input"
          />
        </div>
        <div>
          <Text
            margin="normal"
            variant="outlined"
            label="Módulo do curso"
            name="course_module"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.course_module}
            helperText={errors.course_module?.message}
            id="input"
          />
        </div>

        <div>
          <Button type="submit" variant="contained" color="primary" id="button">
            Enviar
          </Button>
        </div>
      </ThemeProvider>
      {error && <Alert severity="warning">{error}</Alert>}
    </form>
  );
};
export default FormRegister;
