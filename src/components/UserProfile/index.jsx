import Techs from "../Techs";
import "./style.css";
const UserProfile = ({ user, handleUser }) => {
  const { avatar_url, bio, contact, course_module, email, name, techs } = user;
  return (
    <div className="profile-container">
      <section className="info">
        <figure className="profile-img">
          <img
            src={
              avatar_url
                ? avatar_url
                : "https://kenziehub.s3.amazonaws.com/264144652f8bb342348c-KA.png"
            }
            alt={name}
          />
        </figure>
        <div className="data-user">
          <h3>{name}</h3>
          <div className="info-user">
            <p>
              <span>Módulo: </span>
              {course_module}
            </p>
            <p>
              <span>Contato: </span>
              {contact}
            </p>
            <p>
              <span>E-mail: </span>
              {email}
            </p>
          </div>
        </div>
      </section>
      <section className="bio">
        <h4>Bio</h4>
        <div className="bio-container">
          <p>{bio}</p>
        </div>
      </section>
      <section className="techs">
        <Techs techs={techs} handleUser={handleUser} />
      </section>
    </div>
  );
};

export default UserProfile;
