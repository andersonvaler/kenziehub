import React from "react";
import Paper from "@material-ui/core/Paper";
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { Tabs, Tab } from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import PersonIcon from "@material-ui/icons/Person";
import { useHistory } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import "./style.css";

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#031963",
    },
    secondary: {
      main: "#FDFFFF",
    },
  },
});

const Menu = ({ isAuth, setIsAuth }) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const history = useHistory();

  const sendTo = (path) => {
    history.push(path);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const logout = () => {
    sessionStorage.clear();
    setIsAuth(false);
    sendTo("/");
  };

  return (
    <>
      {isAuth ? (
        <>
          <ThemeProvider theme={theme}>
            <Paper square className={classes.root} id="menu">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="secondary"
                aria-label="icon label tabs example"
              >
                <Tab
                  icon={<HomeIcon id="icon" />}
                  label="Home"
                  onClick={() => sendTo("home")}
                  id="menu-item"
                />
                <Tab
                  icon={<PersonIcon id="icon" />}
                  label="Sair"
                  onClick={logout}
                  id="menu-item"
                />
              </Tabs>
            </Paper>
          </ThemeProvider>
        </>
      ) : (
        <>
          <ThemeProvider theme={theme}>
            <Paper square className={classes.root} id="menu">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="secondary"
                aria-label="icon label tabs example"
              >
                <Tab
                  icon={<PersonIcon id="icon" />}
                  label="Login"
                  onClick={() => sendTo("/")}
                  id="menu-item"
                />
                <Tab
                  icon={<PersonAddIcon id="icon" />}
                  label="Cadastro"
                  onClick={() => sendTo("register")}
                  id="menu-item"
                />
              </Tabs>
            </Paper>
          </ThemeProvider>
        </>
      )}
    </>
  );
};

export default Menu;
