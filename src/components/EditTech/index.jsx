import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { useForm } from "react-hook-form";
import { Button, TextField } from "@material-ui/core";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useHistory } from "react-router-dom";
// import "./style.css";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const EditTech = ({ openEdit, setOpenEdit, id, handleUser }) => {
  const history = useHistory();
  const schema = yup.object().shape({
    status: yup.string().required("Campo obrigatório"),
  });
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });
  const classes = useStyles();

  const cancelSubmit = () => {
    setOpenEdit(false);
  };

  const handleForm = (data) => {
    const sessionToken = sessionStorage.getItem("token");
    setOpenEdit(false);
    axios
      .put(`https://kenziehub.me/users/techs/${id}`, data, {
        headers: { Authorization: `Bearer ${JSON.parse(sessionToken)}` },
      })
      .then((response) => {
        handleUser();
      })
      .catch((error) => {
        console.log(error);
      });
    history.push("/home");
  };

  const body = (
    <form
      id="add-tech"
      className={classes.paper}
      onSubmit={handleSubmit(handleForm)}
    >
      <h4 id="add-tech-modal">Editar Tecnologia</h4>
      <TextField
        margin="normal"
        variant="outlined"
        label="Experiência"
        name="status"
        size="small"
        color="primary"
        inputRef={register}
        error={!!errors.title}
        helperText={errors.title?.message}
      />
      <EditTech />
      <div id="buttons">
        <Button
          type="submit"
          variant="contained"
          color="primary"
          id="button-tech"
        >
          Enviar
        </Button>
        <Button
          variant="contained"
          color="secondary"
          id="button-cancel"
          onClick={() => {
            cancelSubmit();
          }}
        >
          Cancelar
        </Button>
      </div>
    </form>
  );

  return (
    <div>
      <Modal
        open={openEdit}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default EditTech;
