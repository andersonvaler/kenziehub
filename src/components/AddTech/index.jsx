import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { useForm } from "react-hook-form";
import { Button, TextField } from "@material-ui/core";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import "./style.css";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    // border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const AddTech = ({ open, setOpen, handleUser }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    status: yup.string().required("Campo obrigatório"),
  });
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });
  const classes = useStyles();

  const cancelSubmit = () => {
    setOpen(false);
  };

  const handleForm = (data) => {
    const sessionToken = sessionStorage.getItem("token");
    setOpen(false);
    axios
      .post("https://kenziehub.me/users/techs", data, {
        headers: { Authorization: `Bearer ${JSON.parse(sessionToken)}` },
      })
      .then((response) => {
        handleUser();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const body = (
    <form
      id="add-tech"
      className={classes.paper}
      onSubmit={handleSubmit(handleForm)}
    >
      <h4 id="add-tech-modal">Adicionar tecnologia</h4>
      <TextField
        margin="normal"
        variant="outlined"
        label="Nome"
        name="title"
        size="small"
        color="primary"
        inputRef={register}
        error={!!errors.title}
        helperText={errors.title?.message}
      />
      <TextField
        margin="normal"
        variant="outlined"
        label="Experiência"
        name="status"
        size="small"
        color="primary"
        inputRef={register}
        error={!!errors.status}
        helperText={errors.status?.message}
      />
      <AddTech />
      <div id="buttons">
        <Button
          type="submit"
          variant="contained"
          color="primary"
          id="button-tech"
        >
          Enviar
        </Button>
        <Button
          variant="contained"
          color="secondary"
          id="button-cancel"
          onClick={() => {
            cancelSubmit();
          }}
        >
          Cancelar
        </Button>
      </div>
    </form>
  );

  return (
    <div>
      <Modal
        open={open}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default AddTech;
