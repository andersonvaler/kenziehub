import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { useState } from "react";
import MuiAlert from "@material-ui/lab/Alert";
import axios from "axios";

import { Button, TextField } from "@material-ui/core";
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0F486B",
    },
    secondary: {
      main: "#031963",
    },
    error: {
      main: "#FF220C",
    },
  },
});
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const FormLogin = () => {
  const history = useHistory();
  const [error, setError] = useState(false);
  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos uma letra maiúscula, uma minúscula, um número e um caracter especial!"
      )
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    axios
      .post("https://kenziehub.me/sessions", data)
      .then((response) => {
        sessionStorage.clear();
        sessionStorage.setItem("token", JSON.stringify(response.data.token));
        reset();
        history.push("/home");
      })
      .catch((error) => {
        const { status, message } = error.response.data;
        if (status === "error") {
          setError(message);
          reset();
        }
      });
  };

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <h2>Login</h2>

      <ThemeProvider theme={theme}>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Email"
            name="email"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
        </div>

        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Senha"
            name="password"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
            type="password"
          />
        </div>

        <div>
          <Button type="submit" variant="contained" color="primary" id="button">
            Enviar
          </Button>
        </div>
      </ThemeProvider>
      {error && <Alert severity="warning">Email e/ou senha inválidos</Alert>}
    </form>
  );
};

export default FormLogin;
