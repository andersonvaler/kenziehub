import "./App.css";
import Routes from "./routes";
import Menu from "./components/Menu";
import { useState } from "react";

function App() {
  const [isAuth, setIsAuth] = useState(false);

  return (
    <div className="App">
      <header>
        <Menu isAuth={isAuth} setIsAuth={setIsAuth} />
      </header>
      <main>
        <Routes setIsAuth={setIsAuth} />
      </main>
    </div>
  );
}

export default App;
