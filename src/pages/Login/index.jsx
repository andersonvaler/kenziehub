import FormLogin from "../../components/FormLogin";

const Login = () => {
  return (
    <div id="form-login">
      <FormLogin />
    </div>
  );
};

export default Login;
