import { useState, useEffect } from "react";
import axios from "axios";
import UserProfile from "../../components/UserProfile";
import { Redirect } from "react-router-dom";

const Home = ({ setIsAuth }) => {
  const [user, setUser] = useState({});
  const [mount, setMount] = useState(true);
  const [token, setToken] = useState(() => {
    const sessionToken = sessionStorage.getItem("token") || "";

    if (!sessionToken) {
      return "";
    }
    setIsAuth(true);
    return JSON.parse(sessionToken);
  });

  const handleUser = () => {
    axios
      .get("https://kenziehub.me/profile", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => setUser(response.data))
      .catch((e) => console.log(e));
    setMount(false);
  };

  useEffect(() => {
    if (mount) {
      handleUser();
    }
  });

  if (!token) {
    setToken("");
    return <Redirect to="/register" />;
  }

  return (
    <div>
      <UserProfile user={user} handleUser={handleUser} />
    </div>
  );
};

export default Home;
