import FormRegister from "../../components/FormRegister";
import "./style.css";

const Register = () => {
  return (
    <div>
      <FormRegister />
    </div>
  );
};

export default Register;
